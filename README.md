# ExchangeRates
This application was developed to display current exchange rates and also historical change rates for given currencies.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Running the application
Run `npm install` in a folder where you wish to setup the project.
After completion, change directory to `exchange-rates` and run `ng serve`.
If the port is taken, run `ng serve --port=4231` or some other free port number.

## Running tests
To run tests use the `ng test` command.
