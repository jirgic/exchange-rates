import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { LatestRates } from '../shared/models/latest-rates.model';
import { Rate } from '../shared/models/rate.model';
import { ApiService } from '../shared/services/api.service';

@Component({
    selector: 'app-single-rate',
    templateUrl: './single-rate.component.html',
    styleUrls: ['./single-rate.component.scss']
})
export class SingleRateComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    currency: string;
    rates: Rate;
    base: string;
    startDate: string;
    endDate: string;
    model: NgbDateStruct;
    rateList: Array<string>;
    constructor(
        private route: ActivatedRoute,
        private http: HttpClient,
        private apiService: ApiService
    ) { }

    ngOnInit(): void {
        this.currency = this.route.snapshot.paramMap.get('id');
        this.retrieveData(undefined, undefined);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    retrieveData(startDate: string, endDate: string, base?: string): void {
        const date: Date = new Date();
        date.setMonth(date.getMonth() - 3);
        const start = startDate ? startDate : date.toJSON().slice(0, 10).replace(/-/g, '-').toString();
        const end = endDate ? endDate : new Date().toJSON().slice(0, 10).replace(/-/g, '-').toString();
        const symbol = this.currency;
        base = base === null || base === undefined ? 'EUR' : base;
        this.subscriptions.push(this.apiService.getLatestRates(base).subscribe((data: LatestRates) => {
            this.rateList = Object.keys(data.rates);
        }));
        this.subscriptions.push(this.apiService.getHistoricalRates(start, end, symbol, base).subscribe((data: LatestRates) => {
            this.rates = data.rates;
            this.base = data.base === 'EUR' ? null : data.base.trim();
            this.startDate = data.start_at;
            this.endDate = data.end_at;
        }));
    }

    onBaseSelection(base): void {
        this.retrieveData(this.startDate, this.endDate, base);
    }

    searchDate(): void {
        this.retrieveData(this.startDate, this.endDate, this.base);
    }

}
