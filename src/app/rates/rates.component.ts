import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LatestRates } from '../shared/models/latest-rates.model';
import { Rate } from '../shared/models/rate.model';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../shared/services/api.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-rates',
    templateUrl: './rates.component.html',
    styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    rates: Rate;
    base = null;
    date: string;
    model: NgbDateStruct;
    constructor(
        private http: HttpClient,
        private toastr: ToastrService,
        private apiService: ApiService
    ) { }

    ngOnInit(): void {
        this.retrieveData(undefined);
        if (!localStorage.getItem('showedToastr')) {
          this.toastr.info('To get currency data for a single currency, press on it`s code, e.g. AUD', undefined, {
            timeOut: 6000,
          });
          localStorage.setItem('showedToastr', 'true');
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    retrieveData(date: string, base?: string): void {
        this.subscriptions.push(this.apiService.getLatestRates(base, date).subscribe((data: LatestRates) => {
            this.rates = data.rates;
            this.base = data.base === 'EUR' ? null : data.base.trim();
            this.date = data.date;
        }));
    }

    onBaseSelection(base): void {
        this.retrieveData(this.date, base);
    }

}
