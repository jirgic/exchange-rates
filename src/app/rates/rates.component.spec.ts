
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, getTestBed, ComponentFixture } from '@angular/core/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { RatesComponent } from './rates.component';

describe('RateComponent', () => {
    let component: RatesComponent;
    let fixture: ComponentFixture<RatesComponent>;
    let li: HTMLElement;
    let table: HTMLElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ RatesComponent ],
        imports: [HttpClientTestingModule, ToastrModule.forRoot()],
        providers: [HttpClientTestingModule, ToastrService ]
      });
      fixture = TestBed.createComponent(RatesComponent);
      component = fixture.componentInstance;
      li = fixture.nativeElement.querySelector('.breadcrumb');
      table = fixture.nativeElement.querySelector('table');

    });

    it('should display the breadcrumb', () => {
        expect(li.textContent).toContain('Currency Exchange Rates');
    });
    it('should display the table', () => {
        expect(table.textContent).toContain('CurrencyRate');
    });
});
