import { Rate } from './rate.model';

export class LatestRates {
    rates: Rate;
    base: string;
    date: string;
    end_at?: string;
    start_at?: string;
}
