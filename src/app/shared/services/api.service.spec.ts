
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { LatestRates } from '../models/latest-rates.model';

describe('ApiService', () => {
  let injector: TestBed;
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
    injector = getTestBed();
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getLatestRates', () => {
    it('should return an Observable<LatestRates>', () => {
      service.getLatestRates('EUR', '09-09-2020').subscribe(rates => {
        expect(rates.base).toBe('EUR');
      });
      const req = httpMock.expectOne(`${service.baseUrl}09-09-2020?base=EUR`);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getHistoricalRates', () => {
    it('should return an Observable<LatestRates>', () => {
      service.getHistoricalRates('01-09-2020', '09-09-2020', 'AUD', 'EUR').subscribe(rates => {
        expect(rates.base).toBe('EUR');
      });
      const req = httpMock.expectOne(`${service.baseUrl}history?start_at=01-09-2020&end_at=09-09-2020&symbols=AUD&base=EUR`);
      expect(req.request.method).toBe('GET');
    });
  });
});
