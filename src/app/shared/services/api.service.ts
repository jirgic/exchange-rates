import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LatestRates } from '../models/latest-rates.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
    baseUrl = 'https://api.exchangeratesapi.io/';
    constructor(
        private http: HttpClient
    ) {}

    /*
    * Retrieves scenario model topology
    */
    getLatestRates(base: string, date?: string): Observable<LatestRates> {
        const url = this.baseUrl + (date ? date : 'latest');
        let params = new HttpParams();
        base = base === null || base === '0: null' ? 'EUR' : base;
        if (base !== undefined) {
            params = params.set('base', base);
        }
        return this.http.get<LatestRates>(url, {params});
    }

    getHistoricalRates(start, end, symbol, base): Observable<LatestRates> {
        const url = this.baseUrl + 'history';
        let params = new HttpParams()
            .set('start_at', start)
            .set('end_at', end)
            .set('symbols', symbol);
        base = base === null || base === undefined ? 'EUR' : base;
        if (base !== undefined) {
            params = params.set('base', base);
        }
        return this.http.get<LatestRates>(url, {params});
    }
}
