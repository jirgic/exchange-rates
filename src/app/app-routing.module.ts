import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RatesComponent } from './rates/rates.component';
import { SingleRateComponent } from './single-rate/single-rate.component';

export const routes: Routes = [
  { path: '', redirectTo: 'rates', pathMatch: 'full'},
  {
    path: 'rates',
    children: [
      { path: ':id', component: SingleRateComponent},
      { path: '', component: RatesComponent }
    ]
  },
  { path: '**', component: RatesComponent}
];

@NgModule({
imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
exports: [RouterModule]
})
export class AppRoutingModule { }
